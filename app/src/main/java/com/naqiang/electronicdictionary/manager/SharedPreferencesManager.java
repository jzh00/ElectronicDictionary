package com.naqiang.electronicdictionary.manager;

import android.content.Context;
import android.content.SharedPreferences;

import com.naqiang.electronicdictionary.application.MyApplication;

import java.util.Map;

/**
 * Author by naqiang, Date on 2019\2\22 0022.
 * PS: Not easy to write code, please indicate.
 * 存储类封装
 */
public class SharedPreferencesManager {

    private static final String NAME = "mysharedprefrences";
    private volatile static SharedPreferencesManager singleton;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public final static String KEY_IP="myip";
    public final static String KEY_FROM="fromlanguage";
    public final static String KEY_TO="tolanguage";
    public final static String KEY_USERNAME="username";
    public final static String KEY_PASSWORD="password";

    private SharedPreferencesManager() {
        sharedPreferences = MyApplication.getContext().getSharedPreferences(NAME,
                Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public static SharedPreferencesManager getSingleTon() {
        if (singleton == null) {
            synchronized (SharedPreferencesManager.class) {
                if (singleton == null) {
                    singleton = new SharedPreferencesManager();
                }
            }
        }
        return singleton;
    }

    /**
     * 存储
     */
    public void put(String key, Object object) {
        if (object instanceof String) {
            editor.putString(key, (String) object);
        } else if (object instanceof Integer) {
            editor.putInt(key, (Integer) object);
        } else if (object instanceof Boolean) {
            editor.putBoolean(key, (Boolean) object);
        } else if (object instanceof Float) {
            editor.putFloat(key, (Float) object);
        } else if (object instanceof Long) {
            editor.putLong(key, (Long) object);
        } else {
            editor.putString(key, object.toString());
        }
        editor.commit();
    }

    /**
     * 获取保存的数据
     */
    public Object getSharedPreference(String key, Object defaultObject) {
        if (defaultObject instanceof String) {
            return sharedPreferences.getString(key, (String) defaultObject);
        } else if (defaultObject instanceof Integer) {
            return sharedPreferences.getInt(key, (Integer) defaultObject);
        } else if (defaultObject instanceof Boolean) {
            return sharedPreferences.getBoolean(key, (Boolean) defaultObject);
        } else if (defaultObject instanceof Float) {
            return sharedPreferences.getFloat(key, (Float) defaultObject);
        } else if (defaultObject instanceof Long) {
            return sharedPreferences.getLong(key, (Long) defaultObject);
        } else {
            return sharedPreferences.getString(key, null);
        }
    }

    /**
     * 移除某个key值已经对应的值
     */
    public void remove(String key) {
        editor.remove(key);
        editor.commit();
    }

    /**
     * 清除所有数据
     */
    public void clear() {
        editor.clear();
        editor.commit();
    }

    /**
     * 查询某个key是否存在
     */
    public Boolean contain(String key) {
        return sharedPreferences.contains(key);
    }

    /**
     * 返回所有的键值对
     */
    public Map<String, ?> getAll() {
        return sharedPreferences.getAll();
    }
}
