package com.naqiang.electronicdictionary.application;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.naqiang.electronicdictionary.data.Constans;
import com.naqiang.electronicdictionary.manager.SharedPreferencesManager;
import com.naqiang.electronicdictionary.util.AppInfoUtils;
import com.youdao.sdk.app.YouDaoApplication;

import static com.naqiang.electronicdictionary.data.Constans.IP;

public class MyApplication extends Application {

    private static MyApplication app;
    private static Context mContext;
    public static Handler mHandler = new Handler(Looper.getMainLooper());

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        mContext = getApplicationContext();
        String signal = AppInfoUtils.getSingInfo(getApplicationContext(), getPackageName(), AppInfoUtils.SHA1);
        Log.i("loplplpkloopo","signal:"+signal);
        //初始化有道SDK
        YouDaoApplication.init(this, "7301e1dafc455e04");
        IP = (String) SharedPreferencesManager.getSingleTon().getSharedPreference(SharedPreferencesManager.KEY_IP, "192.168.1.100");
        Constans.ADDRESS_HEAD = "http://" + IP + ":8080/ElectronicDictionary_Web/";
    }

    /**
     * 单例模式
     *
     * @return
     */
    public static MyApplication getSingeton() {
        return app;
    }


    /**
     * 返回Application的上下文
     *
     * @return
     */
    public static Context getContext() {
        return mContext;
    }

}
