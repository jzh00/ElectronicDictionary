package com.naqiang.electronicdictionary.data;

/**
 * sqlite数据库常量配置
 */
public class TableInfo {

    public static String TABLE_NAME_TRANSLATE() {
        return "translate";
    }
    public static String TABLE_NAME_TRANSLATE_OFFLINE() {
        return "englishwords";
    }

    public static final String ID = "id";  //ID

    public static final String SEARCH = "search";  //搜索的单词或句子

    public static final String RESULT = "result";  //翻译得到的结果

    public static final String FROM_LANGUAGE="from_language";//查询的语种

    public static final String TO_LANGUAGE="to_language";//得到的语种

    public static final String TIME = "time";  //翻译时间

    //离线词典库部分

    public static final String WORD="word";

    public static final String MEANING="meaning";

    public static final String YINBIAO="pronunciation";

    public static String CREAT_TABLE_TRANSLATE() {
        return new StringBuffer().
                append("CREATE TABLE IF NOT EXISTS ").append(TABLE_NAME_TRANSLATE()).
                append("(").
                append(ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT,").
                append(SEARCH).append(" TEXT,").
                append(RESULT).append(" TEXT,").
                append(FROM_LANGUAGE).append(" TEXT,").
                append(TO_LANGUAGE).append(" TEXT,").
                append(TIME).append(" TEXT").
                append(");").toString();
    }

    private static String DROP_TABLE_NAME_TRANSLATE() {
        return "DROP TABLE IF EXISTS " + TABLE_NAME_TRANSLATE();
    }

}
