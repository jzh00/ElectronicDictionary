package com.naqiang.electronicdictionary.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.naqiang.electronicdictionary.application.MyApplication;

/**
 * 数据库访问辅助类
 */
public class DBHelper {

    private static DBHelper dbhelper = null;

    private DatabaseHelper databasehelper = null;

    private DBHelper() {
        this.databasehelper = new DatabaseHelper(MyApplication.getContext());
    }


    public synchronized static DBHelper getInstance() {   //单例模型
        if (dbhelper == null) {
            dbhelper = new DBHelper();
        }
        return dbhelper;
    }

    public SQLiteDatabase getWritableDB() {
        return databasehelper.getWritableDatabase();
    }


    public SQLiteDatabase getReadableDB() {
        return databasehelper.getReadableDatabase();
    }

    public void closeDB() {
        databasehelper.close();
    }

    private class DatabaseHelper extends SQLiteOpenHelper {

        private static final String DATABASE_NAME = "naqiang.db";  //数据库名称
        private static final int DB_VERSION = 1;

        public DatabaseHelper(Context context, String name,
                              CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        public DatabaseHelper(Context context) {
            this(context, DATABASE_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(TableInfo.CREAT_TABLE_TRANSLATE());
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(TableInfo.CREAT_TABLE_TRANSLATE());
        }

    }


}
