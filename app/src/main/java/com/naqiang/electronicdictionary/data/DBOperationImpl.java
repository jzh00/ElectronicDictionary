package com.naqiang.electronicdictionary.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.naqiang.electronicdictionary.application.MyApplication;
import com.naqiang.electronicdictionary.entity.OfflineData;
import com.naqiang.electronicdictionary.entity.UnKnowWord;
import com.naqiang.electronicdictionary.util.LogUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * 此类极为重要，所有的sqlite数据库都在此类里
 * 数据库操作实现类
 */
public class DBOperationImpl {


    private static DBOperationImpl instance;
    private SQLiteDatabase sqLiteDatabase;
    private SQLiteDatabase sqLiteDatabase_offline;
    private AssetsDatabaseManager mg;

    private DBOperationImpl() {
        if (sqLiteDatabase == null) {
            sqLiteDatabase = DBHelper.getInstance().getReadableDB();
            sqLiteDatabase.setLocale(Locale.CHINESE);//设置本地化
        }

        if (mg == null) {
            //初始化离线数据库
            // 初始化，只需要调用一次
            AssetsDatabaseManager.initManager(MyApplication.getContext());
            // 获取管理对象，因为数据库需要通过管理对象才能够获取
            mg = AssetsDatabaseManager.getManager();
            sqLiteDatabase_offline=mg.getDatabase(AssetsDatabaseManager.OFFLINE_DBNAME);
        }
    }

    /**
     * 单例返回数据库操作实现类对象
     */
    public static DBOperationImpl getInstance() {
        if (instance == null) {
            synchronized (DBOperationImpl.class) {
                if (instance == null) {
                    instance = new DBOperationImpl();
                }
            }
        }
        return instance;
    }


    /**
     * 删除生词
     *
     * @param unKnowWord
     * @return
     */
    public boolean deleteUnKnowWord(UnKnowWord unKnowWord) {

        if (unKnowWord == null) {
            return false;
        }
        String DELETE_SQL = TableInfo.TIME + "=?";
        return sqLiteDatabase.delete(TableInfo.TABLE_NAME_TRANSLATE(), DELETE_SQL, new String[]{unKnowWord.addTime}) > 0 ? true : false;
    }


    /**
     * 向数据库中插入生词本
     *
     * @param unKnowWord
     * @return
     */
    public boolean insertUnKnowWord(UnKnowWord unKnowWord) {

        if (unKnowWord == null) {
            return false;
        }
        ContentValues contentValues = getContentValues(unKnowWord);
        long result = sqLiteDatabase.insert(TableInfo.TABLE_NAME_TRANSLATE(), null, contentValues);
        return result > 0 ? true : false;
    }


    /**
     * 获取ContentValues装载数据
     *
     * @param unKnowWord
     * @return
     */
    public ContentValues getContentValues(UnKnowWord unKnowWord) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableInfo.SEARCH, unKnowWord.fromWord);
        contentValues.put(TableInfo.RESULT, unKnowWord.toWord);
        contentValues.put(TableInfo.FROM_LANGUAGE, unKnowWord.fromLanguage);
        contentValues.put(TableInfo.TO_LANGUAGE, unKnowWord.toLanguage);
        contentValues.put(TableInfo.TIME, unKnowWord.addTime);
        return contentValues;
    }

    /**
     * 获取离线词库ContentValues装载数据
     *
     * @param offlineData
     * @return
     */
    public ContentValues getOfflineContentValues(OfflineData offlineData) {
        ContentValues contentValues = new ContentValues();

        if ("英文".equals(offlineData.fromLanguage)) {
            contentValues.put(TableInfo.WORD, offlineData.fromWord);
            contentValues.put(TableInfo.MEANING, offlineData.toWord);
            contentValues.put(TableInfo.YINBIAO, offlineData.yinbiao);
        }
        if("中文".equals(offlineData.fromLanguage)){
            contentValues.put(TableInfo.WORD, offlineData.toWord);
            contentValues.put(TableInfo.MEANING, offlineData.fromWord);
            contentValues.put(TableInfo.YINBIAO, offlineData.yinbiao);
        }
        return contentValues;
    }

    /**
     * 获取单词本所有信息
     *
     * @return
     */
    public List<UnKnowWord> getAllUnKnowWord() {
        List<UnKnowWord> list = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + TableInfo.TABLE_NAME_TRANSLATE() + " ORDER BY " + TableInfo.TIME + " DESC", null);
        while (cursor.moveToNext()) {
            UnKnowWord unKnowWord = new UnKnowWord();
            unKnowWord.fromWord = cursor.getString(cursor.getColumnIndex(TableInfo.SEARCH));
            unKnowWord.toWord = cursor.getString(cursor.getColumnIndex(TableInfo.RESULT));
            unKnowWord.fromLanguage = cursor.getString(cursor.getColumnIndex(TableInfo.FROM_LANGUAGE));
            unKnowWord.toLanguage = cursor.getString(cursor.getColumnIndex(TableInfo.TO_LANGUAGE));
            unKnowWord.addTime = cursor.getString(cursor.getColumnIndex(TableInfo.TIME));
            list.add(unKnowWord);
        }
        cursor.close();
        return list;
    }

    /**
     * 查询离线单词库
     * 离线部分词库音标字段加到了addTime属性里去
     *
     * @return
     */
    public OfflineData getOfflineUnKnowWord(String name, boolean isCh2En) {
        OfflineData offlineData = new OfflineData();
        String str = !isCh2En ? "word" : "meaning";
        String sql = "SELECT * FROM " + TableInfo.TABLE_NAME_TRANSLATE_OFFLINE() + " WHERE " + str + "='" + name + "'";
        Cursor cursor = sqLiteDatabase_offline.rawQuery(sql, null);
        LogUtil.i("ccccccc", sql);
        if (cursor.moveToNext()) {
            offlineData.fromLanguage = isCh2En ? "英文" : "中文";
            offlineData.toLanguage = isCh2En ? "中文" : "英文";
            offlineData.fromWord = cursor.getString(cursor.getColumnIndex(!isCh2En ? TableInfo.WORD : TableInfo.MEANING));
            offlineData.toWord = cursor.getString(cursor.getColumnIndex(!isCh2En ? TableInfo.MEANING : TableInfo.WORD));
            offlineData.yinbiao = cursor.getString(cursor.getColumnIndex(TableInfo.YINBIAO));
        }
        cursor.close();
        return offlineData;
    }

    /**
     * 向离线数据库中插入新的词汇
     *
     * @param offlineData
     * @return
     */
    public boolean insertOfflineDictornay(OfflineData offlineData) {

        if (offlineData == null) {
            return false;
        }
        ContentValues contentValues = getOfflineContentValues(offlineData);
        long result = sqLiteDatabase_offline.insert(TableInfo.TABLE_NAME_TRANSLATE_OFFLINE(), null, contentValues);
        return result > 0 ? true : false;
    }


}
