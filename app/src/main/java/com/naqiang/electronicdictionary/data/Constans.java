package com.naqiang.electronicdictionary.data;

import android.os.Environment;

public class Constans {


    /**
     * 服务器端所在的IP地址，请保证手机和电脑在同一个局域网内（实现方式是：手机和电脑连接同一个无线网，或者手机开热点，电脑连接）
     * IP地址的获取方式，在电脑端DOS界面敲击命令 ipconfig 得到IPV4地址填入下方，重新跑程序，即可连接服务器
     */
    public static  String IP = "192.168.1.101";

    public static  String ADDRESS_HEAD = "http://" + IP + ":8080/ElectronicDictionary_Web/";

    /**
     * 用户名常量，存在登录里的
     */
    public static String CURRENT_USER;
}
