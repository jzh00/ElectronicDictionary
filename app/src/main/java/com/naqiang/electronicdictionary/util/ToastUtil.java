package com.naqiang.electronicdictionary.util;

import android.widget.Toast;

import com.naqiang.electronicdictionary.application.MyApplication;


public class ToastUtil {

    public static void showToast(final String msg) {
        MyApplication.mHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MyApplication.getContext(), msg, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
