package com.naqiang.electronicdictionary.fragment;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.naqiang.electronicdictionary.activity.BaseActivity;


/**
 * Created by naqiang on 2019/3/18.
 * Fragment基类，本项目所有的Fragment集成于此抽象类，类似于BaseActivity，目的为了统一管理
 */
public abstract class BaseFragment extends Fragment implements View.OnClickListener {

    protected BaseActivity mActivity;

    protected Activity mContext;

    protected abstract int setView();

    protected abstract void initView(View view);

    protected abstract void init(Bundle savedInstanceState);

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (BaseActivity) activity;
    }

    @Override
    public void onClick(View v) {
        widgetClick(v);
    }

    /**
     * View点击
     **/
    public abstract void widgetClick(View v);

    /**
     * [设置监听]
     */
    public abstract void setListener();
    /**
     * 做实际的操作
     */
    public abstract void doWork();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(setView(), container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(savedInstanceState);
        initView(view);
        setListener();
        doWork();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    /**
     * 申请指定的权限.
     */
    public void requestPermission(int code, String... permissions) {

        if (Build.VERSION.SDK_INT >= 23) {
            requestPermissions(permissions, code);
        }
    }

    /**
     * 判断是否有指定的权限
     */
    public boolean hasPermission(String... permissions) {

        for (String permisson : permissions) {
            if (ContextCompat.checkSelfPermission(getActivity(), permisson)
                != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            /*case Constant.HARDWEAR_CAMERA_CODE:
                if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    doOpenCamera();
                }
                break;
            case Constant.WRITE_READ_EXTERNAL_CODE:
                if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    doWriteSDCard();
                }
                break;*/
        }
    }

    public void doOpenCamera() {

    }

    public void doWriteSDCard() {

    }
}
