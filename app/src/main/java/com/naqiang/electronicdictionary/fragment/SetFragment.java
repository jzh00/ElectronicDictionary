package com.naqiang.electronicdictionary.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.naqiang.electronicdictionary.R;
import com.naqiang.electronicdictionary.activity.CopyRightActivity;
import com.naqiang.electronicdictionary.activity.LoginRegisterActivity;
import com.naqiang.electronicdictionary.activity.OfflineDictornayExtendActivity;
import com.naqiang.electronicdictionary.data.Constans;
import com.naqiang.electronicdictionary.manager.SharedPreferencesManager;
import com.naqiang.electronicdictionary.util.ToastUtil;

import static com.naqiang.electronicdictionary.data.Constans.IP;

/**
 * Author by naqiang, Date on 2019\3\15 0015.
 * PS: Not easy to write code, please indicate.
 * 设置界面，主要包括登录注册和版权，离线词库扩展什么的
 */
public class SetFragment extends BaseFragment {

    private EditText et_ip_input;
    private LinearLayout ll_login_re, ll_copyright, ll_addofffline;
    private TextView tv_login_re, tv_save;

    @Override
    protected int setView() {
        return R.layout.fragment_set;
    }

    @Override
    protected void initView(View view) {

        et_ip_input = view.findViewById(R.id.et_ip_input);
        et_ip_input.setText((String)SharedPreferencesManager.getSingleTon().getSharedPreference(SharedPreferencesManager.KEY_IP,"192.168.1.100"));
        tv_save = view.findViewById(R.id.tv_save);
        ll_login_re = view.findViewById(R.id.ll_login_re);
        ll_copyright = view.findViewById(R.id.ll_copyright);
        ll_addofffline = view.findViewById(R.id.ll_addofffline);
        tv_login_re = view.findViewById(R.id.tv_login_re);
    }

    @Override
    protected void init(Bundle savedInstanceState) {

    }

    @Override
    public void widgetClick(View v) {

        switch (v.getId()) {
            case R.id.ll_login_re:
                startActivity(new Intent(getActivity(), LoginRegisterActivity.class));
                break;
            case R.id.ll_copyright:
                startActivity(new Intent(getActivity(), CopyRightActivity.class));
                break;
            case R.id.ll_addofffline:
                startActivity(new Intent(getActivity(), OfflineDictornayExtendActivity.class));
                break;
            case R.id.tv_save:
                String ip=et_ip_input.getText().toString();
                if("".equals(ip)){
                    ToastUtil.showToast("ip地址不可以为空");
                    return;
                }
                SharedPreferencesManager.getSingleTon().put(SharedPreferencesManager.KEY_IP,ip);
                ToastUtil.showToast("ip地址修改成功！");
                et_ip_input.setText((String)SharedPreferencesManager.getSingleTon().getSharedPreference(SharedPreferencesManager.KEY_IP,"192.168.1.100"));
                IP = (String) SharedPreferencesManager.getSingleTon().getSharedPreference(SharedPreferencesManager.KEY_IP, "192.168.1.100");
                Constans.ADDRESS_HEAD = "http://" + IP + ":8080/ElectronicDictionary_Web/";
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Constans.CURRENT_USER != null) {
            tv_login_re.setText(Constans.CURRENT_USER);
        } else {
            tv_login_re.setText("登录 / 注册");
        }
    }

    @Override
    public void setListener() {
        ll_login_re.setOnClickListener(this);
        ll_copyright.setOnClickListener(this);
        ll_addofffline.setOnClickListener(this);
        tv_save.setOnClickListener(this);
    }

    @Override
    public void doWork() {

    }
}
