package com.naqiang.electronicdictionary.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SimpleAdapter;

import com.naqiang.electronicdictionary.R;
import com.naqiang.electronicdictionary.adapter.GridViewAdapter;
import com.naqiang.electronicdictionary.manager.SharedPreferencesManager;

/**
 * Author by naqiang, Date on 2019\3\15 0015.
 * PS: Not easy to write code, please indicate.
 * 选择词典界面具体实现
 */
public class ChooseDictonaryFragment extends BaseFragment implements AdapterView.OnItemClickListener {


    private int[] icon = new int[]{R.drawable.cidian, R.drawable.cidian, R.drawable.cidian, R.drawable.cidian,
            R.drawable.cidian, R.drawable.cidian, R.drawable.cidian, R.drawable.cidian,
            R.drawable.cidian, R.drawable.cidian, R.drawable.cidian, R.drawable.cidian};
    private String[] str = new String[]{"中文英文", "英文中文", "中文韩文", "中文日文",
            "中文法文", "中文俄文", "中文德文", "中文阿拉伯文",
            "中文印尼文", "中文葡萄牙文", "中文西班牙文", "中文越南文"};
    private GridView gv_dictornay;
    private GridViewAdapter gridViewAdapter;

    @Override
    protected int setView() {
        return R.layout.fragment_choose_dictonary;
    }

    @Override
    protected void initView(View view) {
        gv_dictornay = view.findViewById(R.id.gv_dictornay);
        gridViewAdapter = new GridViewAdapter(getActivity(), str, icon);
        gridViewAdapter.setIndex(getIndex());
        gv_dictornay.setAdapter(gridViewAdapter);
    }

    @Override
    protected void init(Bundle savedInstanceState) {
    }

    private int getIndex(){
        int j=0;
        String from = (String) SharedPreferencesManager.getSingleTon().getSharedPreference(SharedPreferencesManager.KEY_FROM, "中文");
        String to = (String) SharedPreferencesManager.getSingleTon().getSharedPreference(SharedPreferencesManager.KEY_TO, "英文");
        String dictionary = from + to;
        for (int i = 0; i < str.length; i++) {
            if (str[i].equals(dictionary)) {
                j = i;
            }
        }
        return j;
    }

    @Override
    public void widgetClick(View v) {

    }

    @Override
    public void setListener() {
        gv_dictornay.setOnItemClickListener(this);
    }

    @Override
    public void doWork() {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        String mystr = str[position];
        String from = "中文", to = "英文";
        if (mystr != null) {
            from = mystr.substring(0, 2);
            to = mystr.substring(2, mystr.length());
        }
        SharedPreferencesManager.getSingleTon().put(SharedPreferencesManager.KEY_FROM, from);
        SharedPreferencesManager.getSingleTon().put(SharedPreferencesManager.KEY_TO, to);
        gridViewAdapter.setIndex(getIndex());
        gridViewAdapter.notifyDataSetChanged();
    }
}
