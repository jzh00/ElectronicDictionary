package com.naqiang.electronicdictionary.entity;

/**
 * Author by naqiang, Date on 2019\3\13 0013.
 * PS: Not easy to write code, please indicate.
 * 生词本封装类
 */
public class UnKnowWord {

    public int id;

    public String fromWord;

    public String toWord;

    public String fromLanguage;

    public String toLanguage;

    public String addTime;

    @Override
    public String toString() {
        return "UnKnowWord{" +
                "fromWord='" + fromWord + '\'' +
                ", toWord='" + toWord + '\'' +
                ", fromLanguage='" + fromLanguage + '\'' +
                ", toLanguage='" + toLanguage + '\'' +
                ", addTime='" + addTime + '\'' +
                '}';
    }
}
