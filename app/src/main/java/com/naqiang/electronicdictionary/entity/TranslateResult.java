package com.naqiang.electronicdictionary.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Author by naqiang, Date on 2019\3\13 0013.
 * PS: Not easy to write code, please indicate.
 * 翻译返回的bean
 */
public class TranslateResult {


    /**
     * tSpeakUrl : http://openapi.youdao.com/ttsapi?q=%E5%8F%91%E5%B1%95&langType=zh-CHS&sign=7FDC8B325F26C2773E0DCDDA48CD7C2F&salt=1552455965297&voice=4&format=mp3&appKey=7612d9f575301845
     * web : [{"value":["开发","发展","发育","进化"],"key":"Development"},{"value":["软件开发","软体开发","软件发展","软件升级"],"key":"Software Development"},{"value":["生涯规划","职业发展","生涯发展","事业发展"],"key":"Career Development"}]
     * query : development
     * translation : ["发展"]
     * errorCode : 0
     * dict : {"url":"yddict://m.youdao.com/dict?le=eng&q=development"}
     * webdict : {"url":"http://m.youdao.com/dict?le=eng&q=development"}
     * basic : {"exam_type":["商务英语"],"us-phonetic":"d?'v?l?pm?nt","phonetic":"d?'vel?pm(?)nt","uk-phonetic":"d?'vel?pm(?)nt","uk-speech":"http://openapi.youdao.com/ttsapi?q=development&langType=en&sign=84095B0E395EE676CDE1086516F6DDA1&salt=1552455965297&voice=5&format=mp3&appKey=7612d9f575301845","explains":["n. 发展；开发；发育；住宅小区（专指由同一开发商开发的）；[摄] 显影"],"us-speech":"http://openapi.youdao.com/ttsapi?q=development&langType=en&sign=84095B0E395EE676CDE1086516F6DDA1&salt=1552455965297&voice=6&format=mp3&appKey=7612d9f575301845"}
     * l: en2zh-CHS
     * speakUrl : http://openapi.youdao.com/ttsapi?q=development&langType=en&sign=84095B0E395EE676CDE1086516F6DDA1&salt=1552455965297&voice=4&format=mp3&appKey=7612d9f575301845
     */

    private String tSpeakUrl;
    private String query;
    private String errorCode;
    private DictBean dict;
    private WebdictBean webdict;
    private BasicBean basic;
    private String l;
    private String speakUrl;
    private List<WebBean> web;
    private List<String> translation;

    public String getTSpeakUrl() {
        return tSpeakUrl;
    }

    public void setTSpeakUrl(String tSpeakUrl) {
        this.tSpeakUrl = tSpeakUrl;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public DictBean getDict() {
        return dict;
    }

    public void setDict(DictBean dict) {
        this.dict = dict;
    }

    public WebdictBean getWebdict() {
        return webdict;
    }

    public void setWebdict(WebdictBean webdict) {
        this.webdict = webdict;
    }

    public BasicBean getBasic() {
        return basic;
    }

    public void setBasic(BasicBean basic) {
        this.basic = basic;
    }

    public String getL() {
        return l;
    }

    public void setL(String l) {
        this.l = l;
    }

    public String getSpeakUrl() {
        return speakUrl;
    }

    public void setSpeakUrl(String speakUrl) {
        this.speakUrl = speakUrl;
    }

    public List<WebBean> getWeb() {
        return web;
    }

    public void setWeb(List<WebBean> web) {
        this.web = web;
    }

    public List<String> getTranslation() {
        return translation;
    }

    public void setTranslation(List<String> translation) {
        this.translation = translation;
    }

    public static class DictBean {
        /**
         * url : yddict://m.youdao.com/dict?le=eng&q=development
         */

        private String url;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public static class WebdictBean {
        /**
         * url : http://m.youdao.com/dict?le=eng&q=development
         */

        private String url;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public static class BasicBean {
        /**
         * exam_type : ["商务英语"]
         * us-phonetic : d?'v?l?pm?nt
         * phonetic : d?'vel?pm(?)nt
         * uk-phonetic : d?'vel?pm(?)nt
         * uk-speech : http://openapi.youdao.com/ttsapi?q=development&langType=en&sign=84095B0E395EE676CDE1086516F6DDA1&salt=1552455965297&voice=5&format=mp3&appKey=7612d9f575301845
         * explains : ["n. 发展；开发；发育；住宅小区（专指由同一开发商开发的）；[摄] 显影"]
         * us-speech : http://openapi.youdao.com/ttsapi?q=development&langType=en&sign=84095B0E395EE676CDE1086516F6DDA1&salt=1552455965297&voice=6&format=mp3&appKey=7612d9f575301845
         */

        @SerializedName("us-phonetic")
        private String usphonetic;
        private String phonetic;
        @SerializedName("uk-phonetic")
        private String ukphonetic;
        @SerializedName("uk-speech")
        private String ukspeech;
        @SerializedName("us-speech")
        private String usspeech;
        private List<String> exam_type;
        private List<String> explains;

        public String getUsphonetic() {
            return usphonetic;
        }

        public void setUsphonetic(String usphonetic) {
            this.usphonetic = usphonetic;
        }

        public String getPhonetic() {
            return phonetic;
        }

        public void setPhonetic(String phonetic) {
            this.phonetic = phonetic;
        }

        public String getUkphonetic() {
            return ukphonetic;
        }

        public void setUkphonetic(String ukphonetic) {
            this.ukphonetic = ukphonetic;
        }

        public String getUkspeech() {
            return ukspeech;
        }

        public void setUkspeech(String ukspeech) {
            this.ukspeech = ukspeech;
        }

        public String getUsspeech() {
            return usspeech;
        }

        public void setUsspeech(String usspeech) {
            this.usspeech = usspeech;
        }

        public List<String> getExam_type() {
            return exam_type;
        }

        public void setExam_type(List<String> exam_type) {
            this.exam_type = exam_type;
        }

        public List<String> getExplains() {
            return explains;
        }

        public void setExplains(List<String> explains) {
            this.explains = explains;
        }
    }

    public static class WebBean {
        /**
         * value : ["开发","发展","发育","进化"]
         * key : Development
         */

        private String key;
        private List<String> value;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public List<String> getValue() {
            return value;
        }

        public void setValue(List<String> value) {
            this.value = value;
        }
    }
}
