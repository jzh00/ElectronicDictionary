package com.naqiang.electronicdictionary.entity;

/**
 * Author by naqiang, Date on 2019\3\18 0018.
 * PS: Not easy to write code, please indicate.
 * 每日一句装载类，从服务端获取通过json解析
 */
public class EverydayPerWord {

    private int id;

    private String english;

    private String chinese;

    private int count;

    private String pic;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getChinese() {
        return chinese;
    }

    public void setChinese(String chinese) {
        this.chinese = chinese;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    @Override
    public String toString() {
        return "AWordPerDay [id=" + id + ", english=" + english + ", chinese=" + chinese + ", count=" + count + ", pic="
                + pic + "]";
    }


}
