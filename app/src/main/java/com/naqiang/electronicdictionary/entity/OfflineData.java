package com.naqiang.electronicdictionary.entity;

/**
 * Author by naqiang, Date on 2019\3\17 0017.
 * PS: Not easy to write code, please indicate.
 * 离线词典库信息的封装类
 */
public class OfflineData {

    public int id;

    public String fromWord;

    public String toWord;

    public String fromLanguage;

    public String toLanguage;

    public String yinbiao;

    @Override
    public String toString() {
        return "OfflineData{" +
                "id=" + id +
                ", fromWord='" + fromWord + '\'' +
                ", toWord='" + toWord + '\'' +
                ", fromLanguage='" + fromLanguage + '\'' +
                ", toLanguage='" + toLanguage + '\'' +
                ", yinbiao='" + yinbiao + '\'' +
                '}';
    }
}
