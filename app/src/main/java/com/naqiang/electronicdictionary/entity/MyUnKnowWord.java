package com.naqiang.electronicdictionary.entity;

/**
 * Author by naqiang, Date on 2019\3\20 0020.
 * PS: Not easy to write code, please indicate.
 */
public class MyUnKnowWord {

    public boolean isOnline;

    public UnKnowWord unKnowWord;

    public MyUnKnowWord() {
    }

    public MyUnKnowWord(boolean isOnline, UnKnowWord unKnowWord) {
        this.isOnline = isOnline;
        this.unKnowWord = unKnowWord;
    }

    @Override
    public String toString() {
        return "MyUnKnowWord{" +
                "isOnline=" + isOnline +
                ", unKnowWord=" + unKnowWord +
                '}';
    }
}
