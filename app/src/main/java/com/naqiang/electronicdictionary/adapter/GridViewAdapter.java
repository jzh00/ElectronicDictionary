package com.naqiang.electronicdictionary.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.naqiang.electronicdictionary.R;


/**
 * Author by naqiang, Date on 2019\3\16 0016.
 * PS: Not easy to write code, please indicate.
 * 词典界面的适配器，就是中英文，中法文字典的那个
 */
public class GridViewAdapter extends BaseAdapter {

    private Context context = null;
    private String data[] = null;
    private int imgId[] = null;
    private LayoutInflater inflater;

    private int index;

    //构造方法
    public GridViewAdapter(Context context, String[] data, int[] imgId) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.data = data;
        this.imgId = imgId;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        Holder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.item_layout, null);
            holder = new Holder();
            holder.iv_dictornay_icon = (ImageView) view.findViewById(R.id.iv_dictornay_icon);
            holder.tv_dictornay_name = (TextView) view.findViewById(R.id.tv_dictornay_name);
            holder.rl_gridview = (RelativeLayout) view.findViewById(R.id.rl_gridview);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }
        holder.tv_dictornay_name.setText(data[position]);
        holder.iv_dictornay_icon.setImageResource(imgId[position]);
            holder.rl_gridview.setBackgroundColor(context.getResources().getColor(position == index?R.color.A11:R.color.A2));
        return view;
    }


    private class Holder {

        RelativeLayout rl_gridview;
        ImageView iv_dictornay_icon;
        TextView tv_dictornay_name;

        public ImageView getItem_img() {
            return iv_dictornay_icon;
        }

        public void setItem_img(ImageView item_img) {
            this.iv_dictornay_icon = item_img;
        }

        public TextView getItem_tex() {
            return tv_dictornay_name;
        }

        public void setItem_tex(TextView item_tex) {
            this.tv_dictornay_name = item_tex;
        }
    }
}
