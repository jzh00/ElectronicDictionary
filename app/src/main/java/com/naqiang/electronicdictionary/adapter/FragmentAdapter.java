package com.naqiang.electronicdictionary.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import java.util.List;

/**
 * Fragment适配器，使用ViewPage+Fragment形式实现离线翻译和在线翻译的切换
 */
public class FragmentAdapter extends FragmentPagerAdapter {

    //存放fragment的集合
    private List<Fragment> mFragments;

    private int position;

    public FragmentAdapter(FragmentManager fm, List<Fragment> mFragments) {
        super(fm);
        this.mFragments = mFragments;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    //用于区分具体属于哪个fragment
    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        this.position = position;
        super.setPrimaryItem(container, position, object);
    }

    public int getCurrentFragmentIndex() {
        return this.position;
    }
}
