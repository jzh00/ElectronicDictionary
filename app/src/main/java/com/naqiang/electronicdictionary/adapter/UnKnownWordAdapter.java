package com.naqiang.electronicdictionary.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.naqiang.electronicdictionary.R;
import com.naqiang.electronicdictionary.entity.MyUnKnowWord;
import com.naqiang.electronicdictionary.entity.UnKnowWord;

import java.util.List;

/**
 * Author by naqiang, Date on 2019\3\13 0013.
 * PS: Not easy to write code, please indicate.
 * 生词本的ListView的适配器
 */
public class UnKnownWordAdapter extends BaseAdapter {

    private List<MyUnKnowWord> mList;
    private Context context;
    private LayoutInflater inflater;

    public UnKnownWordAdapter(List<MyUnKnowWord> mList, Context context) {
        if (mList == null) {
            return;
        }
        this.mList = mList;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    public interface OnUnknowNoteSignDeletedLIstener {
        void onUnknowSignDeleted(int position, TextView textView);
    }

    private OnUnknowNoteSignDeletedLIstener onUnknowNoteSignDeletedLIstener;

    public void setOnUnknowNoteSignDeletedLIstener(OnUnknowNoteSignDeletedLIstener onUnknowNoteSignDeletedLIstener) {
        this.onUnknowNoteSignDeletedLIstener = onUnknowNoteSignDeletedLIstener;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.lv_item_set, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        MyUnKnowWord myUnKnowWord = mList.get(position);
        if(myUnKnowWord==null){
            return convertView;
        }
        holder.playContainer.setBackgroundColor(context.getResources().getColor(myUnKnowWord.isOnline?R.color.A11:R.color.A2));
        UnKnowWord unKnowWord=myUnKnowWord.unKnowWord;
        holder.tv_from_language.setText(unKnowWord.fromLanguage);
        holder.tv_from_word.setText(unKnowWord.fromWord);
        holder.tv_toLanguage.setText(unKnowWord.toLanguage);
        holder.tv_toWord.setText(unKnowWord.toWord);
        holder.tv_commentItemTime.setText(unKnowWord.addTime);
        final TextView textView = holder.tv_delete;
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onUnknowNoteSignDeletedLIstener != null) {
                    onUnknowNoteSignDeletedLIstener.onUnknowSignDeleted(position, textView);
                }
            }
        });
        return convertView;
    }

    class ViewHolder {

        TextView tv_from_word;
        TextView tv_from_language;
        TextView tv_toWord;
        TextView tv_toLanguage;
        TextView tv_commentItemTime;
        TextView tv_delete;
        LinearLayout playContainer;

        public ViewHolder(View view) {
            tv_from_word = (TextView) view.findViewById(R.id.tv_from_word);
            tv_from_language = (TextView) view.findViewById(R.id.tv_from_language);
            tv_toWord = (TextView) view.findViewById(R.id.tv_toWord);
            tv_toLanguage = (TextView) view.findViewById(R.id.tv_toLanguage);
            tv_commentItemTime = (TextView) view.findViewById(R.id.tv_commentItemTime);
            tv_delete = (TextView) view.findViewById(R.id.tv_delete);
            playContainer=view.findViewById(R.id.playContainer);
        }
    }
}
