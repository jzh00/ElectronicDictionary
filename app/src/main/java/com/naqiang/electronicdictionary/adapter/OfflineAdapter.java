package com.naqiang.electronicdictionary.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.naqiang.electronicdictionary.R;
import com.naqiang.electronicdictionary.entity.OfflineData;

import java.util.List;


/**
 * Author by naqiang, Date on 2019\3\17 0017.
 * PS: Not easy to write code, please indicate.
 * 离线查询结果列表的适配器
 */
public class OfflineAdapter extends BaseAdapter {

    private List<OfflineData> mList;
    private Context context;
    private LayoutInflater inflater;

    public OfflineAdapter(List<OfflineData> mList, Context context) {
        if (mList == null || context == null) {
            return;
        }
        this.mList = mList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }


    public interface OnOffLineDataListener {
        void onOffLineDataAddNote(int position, TextView textView);
    }

    private OnOffLineDataListener onOffLineDataListener;

    public void setOnOffLineDataListener(OnOffLineDataListener onOffLineDataListener) {
        this.onOffLineDataListener = onOffLineDataListener;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.translate_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        OfflineData offlineData = mList.get(position);
        holder.commentItemContent.setText(offlineData.fromWord);
        holder.translateText.setText(offlineData.toWord);
        holder.more.setVisibility(View.GONE);
        final TextView textView = holder.tv_addnote;
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onOffLineDataListener != null) {
                    onOffLineDataListener.onOffLineDataAddNote(position, textView);
                }
            }
        });
        return convertView;
    }

    class ViewHolder {

        TextView commentItemContent;
        TextView translateText;
        TextView tv_addnote;
        TextView commentItemTime;
        ImageView more;

        public ViewHolder(View view) {
            commentItemContent = (TextView) view.findViewById(R.id.commentItemContent);
            translateText = (TextView) view.findViewById(R.id.translateText);
            tv_addnote = (TextView) view.findViewById(R.id.tv_addnote);
            commentItemTime = (TextView) view.findViewById(R.id.commentItemTime);
            more=(ImageView)view.findViewById(R.id.more);
        }
    }
}
