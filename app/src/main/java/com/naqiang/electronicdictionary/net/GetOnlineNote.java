package com.naqiang.electronicdictionary.net;

import com.naqiang.electronicdictionary.entity.UnKnowWord;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Author by jzh, Date on 2019\3\16 0016.
 * PS: Not easy to write code, please indicate.
 * 获取服务器端的生词本
 */
public interface GetOnlineNote {

    @GET("GetAllNote")
    Observable<List<UnKnowWord>> getCall(@Query("username") String name);
}
