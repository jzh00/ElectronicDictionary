package com.naqiang.electronicdictionary.net;

import com.naqiang.electronicdictionary.entity.EverydayPerWord;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Author by naqiang, Date on 2019\3\18 0018.
 * PS: Not easy to write code, please indicate.
 * 获取每日一句的数据接口
 */
public interface GetEverydayPerWord {

    @GET("OneWordPerDay")
    Observable<EverydayPerWord> getCall();
}
