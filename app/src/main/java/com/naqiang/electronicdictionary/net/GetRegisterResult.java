package com.naqiang.electronicdictionary.net;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Author by naqiang, Date on 2019\3\18 0018.
 * PS: Not easy to write code, please indicate.
 * 注册接口
 */
public interface GetRegisterResult {

    @GET("Register")
    Observable<String> getCall(@Query("name") String name, @Query("pwd") String pwd);
}
