package com.naqiang.electronicdictionary.net;

import com.naqiang.electronicdictionary.entity.EverydayPerWord;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Author by naqiang, Date on 2019\3\20 0020.
 * PS: Not easy to write code, please indicate.
 */
public interface DeleteOnlineNote {
    @GET("DeleteNote")
    Observable<String> deleteNote(@Query("id") int id);

}
