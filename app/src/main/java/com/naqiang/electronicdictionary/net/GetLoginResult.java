package com.naqiang.electronicdictionary.net;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Author by naqiang, Date on 2019\3\16 0016.
 * PS: Not easy to write code, please indicate.
 * 获取登陆结果的接口
 */
public interface GetLoginResult {

    @GET("Login")
    Observable<String> getCall(@Query("name") String name, @Query("pwd") String pwd);
    // 注解里传入 网络请求 的部分URL地址
    // Retrofit把网络请求的URL分成了两部分：一部分放在Retrofit对象里，另一部分放在网络请求接口里
    // 如果接口里的url是一个完整的网址，那么放在Retrofit对象里的URL可以忽略
    // 采用Observable<...>接口
    // getCall()是接受网络请求数据的方法
}
