package com.naqiang.electronicdictionary.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.naqiang.electronicdictionary.R;
import com.naqiang.electronicdictionary.manager.AppManager;

/**
 * 版权所有界面
 */
public class CopyRightActivity extends BaseActivity {

    private LinearLayout ll_topbar_back;

    private TextView tv_topbar_title;

    @Override
    public void widgetClick(View v) {

        switch (v.getId()){
            case R.id.ll_topbar_back:
                AppManager.getAppManager().finishActivity(this);
                finish();
                break;
        }
    }

    @Override
    protected void init() {

    }

    @Override
    public int bindLayout() {
        return R.layout.activity_copy_right;
    }

    @Override
    public void initView(View view) {
        ll_topbar_back = findViewById(R.id.ll_topbar_back);
        tv_topbar_title = findViewById(R.id.tv_topbar_title);
        tv_topbar_title.setText("版权");
    }

    @Override
    public void setListener() {

        ll_topbar_back.setOnClickListener(this);
    }

    @Override
    public void doBusiness(Context mContext) {

    }
}
