package com.naqiang.electronicdictionary.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.naqiang.electronicdictionary.R;
import com.naqiang.electronicdictionary.data.DBOperationImpl;
import com.naqiang.electronicdictionary.entity.OfflineData;
import com.naqiang.electronicdictionary.manager.AppManager;

/**
 * 离线词典扩展
 */
public class OfflineDictornayExtendActivity extends BaseActivity {


    private LinearLayout ll_topbar_back;
    private TextView tv_topbar_title;
    private EditText et_english_input, et_meaning_input, et_fayin_input;
    private Button btn_save;
    private DBOperationImpl dbOperation;

    @Override
    public void widgetClick(View v) {

        switch (v.getId()) {
            case R.id.btn_save:
                String english = et_english_input.getText().toString();
                String chinese = et_meaning_input.getText().toString();
                if ("".equals(english) | "".equals(chinese)) {
                    showToast("输入错误!");
                    return;
                }
                OfflineData offlineData = new OfflineData();
                offlineData.fromLanguage = "英文";
                offlineData.toLanguage = "中文";
                offlineData.fromWord = english;
                offlineData.toWord = chinese;
                offlineData.yinbiao = et_fayin_input.getText().toString();
                boolean flag = dbOperation.insertOfflineDictornay(offlineData);
                showToast(flag ? "加入成功！" : "加入失败!");
                break;
            case R.id.ll_topbar_back:
                AppManager.getAppManager().finishActivity(this);
                finish();
                break;
        }
    }

    @Override
    protected void init() {
        dbOperation = DBOperationImpl.getInstance();
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_offline_dictornay_extend;
    }

    @Override
    public void initView(View view) {
        ll_topbar_back = findViewById(R.id.ll_topbar_back);
        tv_topbar_title = findViewById(R.id.tv_topbar_title);
        et_english_input = findViewById(R.id.et_english_input);
        et_meaning_input = findViewById(R.id.et_meaning_input);
        et_fayin_input = findViewById(R.id.et_fayin_input);
        btn_save = findViewById(R.id.btn_save);
    }

    @Override
    public void setListener() {
        btn_save.setOnClickListener(this);
        ll_topbar_back.setOnClickListener(this);
    }

    @Override
    public void doBusiness(Context mContext) {

    }
}
