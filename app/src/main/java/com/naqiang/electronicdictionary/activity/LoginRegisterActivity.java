package com.naqiang.electronicdictionary.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.naqiang.electronicdictionary.R;
import com.naqiang.electronicdictionary.data.Constans;
import com.naqiang.electronicdictionary.manager.AppManager;
import com.naqiang.electronicdictionary.manager.SharedPreferencesManager;
import com.naqiang.electronicdictionary.net.GetLoginResult;
import com.naqiang.electronicdictionary.net.GetRegisterResult;
import com.naqiang.electronicdictionary.util.ToastUtil;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 登录注册界面
 */
public class LoginRegisterActivity extends BaseActivity {

    private LinearLayout ll_topbar_back;
    private TextView tv_topbar_title,tv_register;
    private EditText login_edit_account, login_edit_pwd,login_edit_pwd_repeat;
    private Button login_btn_login;
    private CheckBox Login_Remember;
    private String usernmae = "", password = "";
    private boolean isLogin=true;

    @Override
    public void widgetClick(View v) {

        switch (v.getId()) {
            case R.id.login_btn_login:
                if(isLogin) {
                    login();
                }else{
                    register();
                }
                break;
            case R.id.ll_topbar_back:
                AppManager.getAppManager().finishActivity(this);
                break;
            case R.id.tv_register:
                if(isLogin){
                    login_btn_login.setText("注册");
                    tv_register.setText("登录 >>");
                    login_edit_pwd_repeat.setVisibility(View.VISIBLE);
                }else{
                    login_btn_login.setText("登录");
                    tv_register.setText("注册 >>");
                    login_edit_pwd_repeat.setVisibility(View.GONE);
                }
                isLogin=!isLogin;
                break;
        }
    }

    /**
     * 注册方法
     */
    private void register() {

        final String account = login_edit_account.getText().toString();
        String pwd = login_edit_pwd.getText().toString();
        String repwd=login_edit_pwd_repeat.getText().toString();
        if ("".equals(account)) {
            showToast("用户名不能为空！");
            return;
        }
        if ("".equals(pwd)) {
            showToast("密码不能为空！");
            return;
        }
        if(!repwd.equals(pwd)){
            showToast("请保证两次密码输入一致！");
            return;
        }
        if (Login_Remember.isChecked()) {
            SharedPreferencesManager.getSingleTon().put(SharedPreferencesManager.KEY_USERNAME, account);
            SharedPreferencesManager.getSingleTon().put(SharedPreferencesManager.KEY_PASSWORD, pwd);
        }
        //创建Retrofit对象
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constans.ADDRESS_HEAD) // 设置 网络请求 Url
                .addConverterFactory(GsonConverterFactory.create()) //设置使用Gson解析(记得加入依赖)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) // 支持RxJava
                .build();

        // 创建 网络请求接口 的实例
        final GetRegisterResult request = retrofit.create(GetRegisterResult.class);

        // 采用Observable<...>形式 对 网络请求 进行封装
        Observable<String> observable = request.getCall(account, pwd);

        // 发送网络请求
        observable.subscribeOn(Schedulers.io())               // 在IO线程进行网络请求
                .observeOn(AndroidSchedulers.mainThread())  // 回到主线程 处理请求结果
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "开始采用subscribe连接");
                    }

                    @Override
                    public void onNext(String result) {
                        // 对返回的数据进行处理
                        Log.i("kkikkikkk", result);
                        if ("1".equals(result)) {
                            showToast("注册成功！");
                            login_btn_login.setText("登录");
                            login_edit_pwd_repeat.setVisibility(View.GONE);
                            isLogin=true;
                        } else if ("0".equals(result)) {
                            showToast("注册失败！");
                        }else if ("-1".equals(result)) {
                            showToast("用户已存在！");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "请求失败");
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "请求成功");
                    }
                });
    }

    /**
     * 登陆方法
     */
    private void login() {
        final String account = login_edit_account.getText().toString();
        String pwd = login_edit_pwd.getText().toString();
        if ("".equals(account)) {
            showToast("用户名不能为空！");
            return;
        }
        if ("".equals(pwd)) {
            showToast("密码不能为空！");
            return;
        }
        if (Login_Remember.isChecked()) {
            SharedPreferencesManager.getSingleTon().put(SharedPreferencesManager.KEY_USERNAME, account);
            SharedPreferencesManager.getSingleTon().put(SharedPreferencesManager.KEY_PASSWORD, pwd);
        }
        //创建Retrofit对象
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constans.ADDRESS_HEAD) // 设置 网络请求 Url
                .addConverterFactory(GsonConverterFactory.create()) //设置使用Gson解析(记得加入依赖)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) // 支持RxJava
                .build();

        // 创建 网络请求接口 的实例
        final GetLoginResult request = retrofit.create(GetLoginResult.class);

        // 采用Observable<...>形式 对 网络请求 进行封装
        Observable<String> observable = request.getCall(account, pwd);

        // 发送网络请求
        observable.subscribeOn(Schedulers.io())               // 在IO线程进行网络请求
                .observeOn(AndroidSchedulers.mainThread())  // 回到主线程 处理请求结果
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "开始采用subscribe连接");
                    }

                    @Override
                    public void onNext(String result) {
                        // 对返回的数据进行处理
                        Log.i("kkikkikkk", result);
                        if ("1".equals(result)) {
                            showToast("登录成功！");
                            Constans.CURRENT_USER = account;
                            AppManager.getAppManager().finishActivity(LoginRegisterActivity.this);
                            finish();
                        } else if ("0".equals(result)) {
                            showToast("登录失败！");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "请求失败");
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "请求成功");
                    }
                });
    }

    @Override
    protected void init() {
        usernmae = (String) SharedPreferencesManager.getSingleTon().getSharedPreference(SharedPreferencesManager.KEY_USERNAME, "");
        password = (String) SharedPreferencesManager.getSingleTon().getSharedPreference(SharedPreferencesManager.KEY_PASSWORD, "");
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_login_register;
    }

    @Override
    public void initView(View view) {
        login_edit_pwd_repeat=findViewById(R.id.login_edit_pwd_repeat);
        Login_Remember = findViewById(R.id.Login_Remember);
        Login_Remember.setChecked(true);
        ll_topbar_back = findViewById(R.id.ll_topbar_back);
        tv_topbar_title = findViewById(R.id.tv_topbar_title);
        tv_topbar_title.setText("登录");
        login_edit_account = findViewById(R.id.login_edit_account);
        login_edit_pwd = findViewById(R.id.login_edit_pwd);
        login_btn_login = findViewById(R.id.login_btn_login);
        tv_register = findViewById(R.id.tv_register);
        if (!"".equals(usernmae)) {
            login_edit_account.setText(usernmae);
        }
        if (!"".equals(password)) {
            login_edit_pwd.setText(password);
        }
    }

    @Override
    public void setListener() {
        login_btn_login.setOnClickListener(this);
        tv_register.setOnClickListener(this);
        ll_topbar_back.setOnClickListener(this);
    }

    @Override
    public void doBusiness(Context mContext) {

    }
}
