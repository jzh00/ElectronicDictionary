package com.naqiang.electronicdictionary.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.naqiang.electronicdictionary.R;
import com.naqiang.electronicdictionary.application.MyApplication;
import com.naqiang.electronicdictionary.manager.AppManager;

import java.lang.reflect.Method;

/**
 * 欢迎界面
 */
public class WelcomeActivity extends BaseActivity {

    @Override
    public void widgetClick(View v) {

    }

    @Override
    protected void init() {

    }

    @Override
    public int bindLayout() {
        return R.layout.activity_welcome;
    }

    @Override
    public void initView(View view) {

    }

    @Override
    public void setListener() {

    }

    @Override
    public void doBusiness(Context mContext) {

        checkIfCPUx86();
        MyApplication.mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(EverydayPerWordActivity.class);
                AppManager.getAppManager().finishActivity(WelcomeActivity.this);
            }
        }, 3000);
    }

    public static boolean checkIfCPUx86() {
        //1. Check CPU architecture: arm or x86
        if (getSystemProperty("ro.product.cpu.abi", "arm").contains("x86")) {
            //The CPU is x86
            return true;
        } else {
            return false;
        }
    }

    private static String getSystemProperty(String key, String defaultValue) {
        String value = defaultValue;
        try {
            Class<?> clazz = Class.forName("android.os.SystemProperties");
            Method get = clazz.getMethod("get", String.class, String.class);
            value = (String) (get.invoke(clazz, key, ""));
            Log.i("youdao", "CPU类型--------------->" + value);
        } catch (Exception e) {
        }

        return value;
    }
}
