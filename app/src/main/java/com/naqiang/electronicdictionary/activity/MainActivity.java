package com.naqiang.electronicdictionary.activity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.naqiang.electronicdictionary.data.AssetsDatabaseManager;
import com.naqiang.electronicdictionary.R;
import com.naqiang.electronicdictionary.fragment.ChooseDictonaryFragment;
import com.naqiang.electronicdictionary.fragment.NoteFragment;
import com.naqiang.electronicdictionary.fragment.SetFragment;
import com.naqiang.electronicdictionary.fragment.TranslateFragment;

/**
 * 主界面
 * 包括 主菜单界面，供用户选择要执行的操作界面。
 * 词典界面，供用户选择合适的词典查词解释。
 * 翻译界面，供用户进行句子的翻译。
 * 单词本界面，供用户学习加入单词本的生疏词汇。
 * 离线词典界面，供用户使用柯林斯词典进行查词解释。
 */
public class MainActivity extends BaseActivity {

    private View top;
    private LinearLayout ll_dictornay, ll_translate, ll_note, ll_set, ll_topbar_back;
    private ImageView iv_dictornay, iv_translate, iv_note, iv_set;
    private TextView tv_topbar_title;
    private FragmentManager fragmentManager;
    private Fragment fragment_choose, fragment_note, fragment_translate, fragment_set;

    @Override
    public void widgetClick(View v) {

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (v.getId()) {
            case R.id.ll_dictornay:
                top.setVisibility(View.VISIBLE);
                tv_topbar_title.setText(getResources().getString(R.string.dictionary));
                iv_dictornay.setBackgroundResource(R.drawable.dict);
                iv_translate.setBackgroundResource(R.drawable.translate);
                iv_note.setBackgroundResource(R.drawable.note);
                iv_set.setBackgroundResource(R.drawable.set);
                hideFragment(fragment_note, fragmentTransaction);
                hideFragment(fragment_translate, fragmentTransaction);
                hideFragment(fragment_set, fragmentTransaction);
                //将homefragment显示到用户面前
                if (fragment_choose == null) {
                    fragment_choose = new ChooseDictonaryFragment();
                    fragmentTransaction.add(R.id.content_layout, fragment_choose);
                } else {
                    fragmentTransaction.show(fragment_choose);
                }
                break;
            case R.id.ll_translate:
                top.setVisibility(View.GONE);
                tv_topbar_title.setText(getResources().getString(R.string.translate));
                iv_dictornay.setBackgroundResource(R.drawable.dict);
                iv_translate.setBackgroundResource(R.drawable.translate);
                iv_note.setBackgroundResource(R.drawable.note);
                iv_set.setBackgroundResource(R.drawable.set);
                hideFragment(fragment_note, fragmentTransaction);
                hideFragment(fragment_choose, fragmentTransaction);
                hideFragment(fragment_set, fragmentTransaction);
                //将homefragment显示到用户面前
                if (fragment_translate == null) {
                    fragment_translate = new TranslateFragment();
                    fragmentTransaction.add(R.id.content_layout, fragment_translate);
                } else {
                    fragmentTransaction.show(fragment_translate);
                }
                break;
            case R.id.ll_note:
                top.setVisibility(View.VISIBLE);
                tv_topbar_title.setText(getResources().getString(R.string.note));
                iv_dictornay.setBackgroundResource(R.drawable.dict);
                iv_translate.setBackgroundResource(R.drawable.translate);
                iv_note.setBackgroundResource(R.drawable.note);
                iv_set.setBackgroundResource(R.drawable.set);
                hideFragment(fragment_translate, fragmentTransaction);
                hideFragment(fragment_choose, fragmentTransaction);
                hideFragment(fragment_set, fragmentTransaction);
                //将homefragment显示到用户面前
                if (fragment_note == null) {
                    fragment_note = new NoteFragment();
                    fragmentTransaction.add(R.id.content_layout, fragment_note);
                } else {
                    fragmentTransaction.show(fragment_note);
                }
                break;
            case R.id.ll_set:
                top.setVisibility(View.VISIBLE);
                tv_topbar_title.setText(getResources().getString(R.string.set));
                iv_dictornay.setBackgroundResource(R.drawable.dict);
                iv_translate.setBackgroundResource(R.drawable.translate);
                iv_note.setBackgroundResource(R.drawable.note);
                iv_set.setBackgroundResource(R.drawable.set);
                hideFragment(fragment_note, fragmentTransaction);
                hideFragment(fragment_choose, fragmentTransaction);
                hideFragment(fragment_translate, fragmentTransaction);
                //将homefragment显示到用户面前
                if (fragment_set == null) {
                    fragment_set = new SetFragment();
                    fragmentTransaction.add(R.id.content_layout, fragment_set);
                } else {
                    fragmentTransaction.show(fragment_set);
                }
                break;
        }
        fragmentTransaction.commit();
    }

    @Override
    protected void init() {

    }

    @Override
    public int bindLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void initView(View view) {

        top = findViewById(R.id.top);
        ll_topbar_back = findViewById(R.id.ll_topbar_back);
        ll_topbar_back.setVisibility(View.GONE);
        tv_topbar_title = findViewById(R.id.tv_topbar_title);
        ll_dictornay = findViewById(R.id.ll_dictornay);
        ll_translate = findViewById(R.id.ll_translate);
        ll_note = findViewById(R.id.ll_note);
        ll_set = findViewById(R.id.ll_set);
        iv_dictornay = findViewById(R.id.iv_dictornay);
        iv_translate = findViewById(R.id.iv_translate);
        iv_note = findViewById(R.id.iv_note);
        iv_set = findViewById(R.id.iv_set);
    }

    @Override
    public void setListener() {

        /*为四个按钮添加点击监听*/
        ll_dictornay.setOnClickListener(this);
        ll_translate.setOnClickListener(this);
        ll_note.setOnClickListener(this);
        ll_set.setOnClickListener(this);
    }

    @Override
    public void doBusiness(Context mContext) {

        //添加要显示的默认的Fragent
        fragment_translate = new TranslateFragment();
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        tv_topbar_title.setText(getResources().getString(R.string.translate));
        top.setVisibility(View.GONE);
        fragmentTransaction.replace(R.id.content_layout, fragment_translate);
        fragmentTransaction.commit();
    }

    /**
     * 用来隐藏具体的Fragment
     *
     * @param fragment
     * @param fragmentTransaction
     */
    private void hideFragment(Fragment fragment, FragmentTransaction fragmentTransaction) {

        if (fragment != null) {
            fragmentTransaction.hide(fragment);
        }
    }

}
