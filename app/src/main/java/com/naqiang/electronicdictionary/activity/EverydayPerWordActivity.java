package com.naqiang.electronicdictionary.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.naqiang.electronicdictionary.R;
import com.naqiang.electronicdictionary.adapter.UnKnownWordAdapter;
import com.naqiang.electronicdictionary.application.MyApplication;
import com.naqiang.electronicdictionary.data.Constans;
import com.naqiang.electronicdictionary.entity.EverydayPerWord;
import com.naqiang.electronicdictionary.entity.UnKnowWord;
import com.naqiang.electronicdictionary.fragment.NoteFragment;
import com.naqiang.electronicdictionary.net.GetEverydayPerWord;
import com.naqiang.electronicdictionary.net.GetOnlineNote;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.glide.transformations.BlurTransformation;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 每日一句界面
 */
public class EverydayPerWordActivity extends BaseActivity {

    private ImageView iv_album;

    private TextView tv_chinese, tv_english;

    private static boolean flag = true;

    @Override
    public void widgetClick(View v) {

        switch (v.getId()) {
            case R.id.iv_album:
                flag = false;
                startActivity(MainActivity.class);
                break;
        }
    }

    @Override
    protected void init() {

    }

    @Override
    public int bindLayout() {
        return R.layout.activity_everyday_per_word;
    }

    @Override
    public void initView(View view) {

        iv_album = findViewById(R.id.iv_album);
        tv_chinese = findViewById(R.id.tv_chinese);
        tv_english = findViewById(R.id.tv_english);
    }

    @Override
    public void setListener() {
        iv_album.setOnClickListener(this);
    }

    @Override
    public void doBusiness(Context mContext) {

        MyApplication.mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (flag)
                    startActivity(MainActivity.class);
            }
        }, 20 * 1000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getEverydayPerWord();
    }

    /**
     * 从网络获取每日一句
     */
    private void getEverydayPerWord() {
        //创建Retrofit对象
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constans.ADDRESS_HEAD) // 设置 网络请求 Url
                .addConverterFactory(GsonConverterFactory.create()) //设置使用Gson解析(记得加入依赖)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) // 支持RxJava
                .build();

        // 创建 网络请求接口 的实例
        final GetEverydayPerWord request = retrofit.create(GetEverydayPerWord.class);

        // 采用Observable<...>形式 对 网络请求 进行封装
        Observable<EverydayPerWord> observable = request.getCall();

        // 发送网络请求
        observable.subscribeOn(Schedulers.io())               // 在IO线程进行网络请求
                .observeOn(AndroidSchedulers.mainThread())  // 回到主线程 处理请求结果
                .subscribe(new Observer<EverydayPerWord>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "开始采用subscribe连接");
                    }

                    @Override
                    public void onNext(EverydayPerWord result) {
                        Log.d("ppppppp", result.toString());
                        // 对返回的数据进行处理
                        Glide.with(EverydayPerWordActivity.this)
                                .load(result.getPic())
                                .error(R.drawable.error)
                                .crossFade(1000)
                                .bitmapTransform(new BlurTransformation(EverydayPerWordActivity.this, 10, 4))  // “23”：设置模糊度(在0.0到25.0之间)，默认”25";"4":图片缩放比例,默认“1”。
                                .into(iv_album);
                        tv_chinese.setText(result.getChinese());
                        tv_english.setText(result.getEnglish());
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "请求失败");
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "请求成功");
                    }
                });
    }
}
